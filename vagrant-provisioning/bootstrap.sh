#!/usr/bin/env bash

HOST_NAME=$1
HOST_ALIAS=$2
HOST_IP=$3
MYSQL_VERSION=5.7

echo "Host name: $HOST_NAME"
echo "Host alias: $HOST_ALIAS"
echo "Host ip: $HOST_IP"

echo "=================================================="
echo "Setting up Magento test environment"
echo "- Ubuntu 14.04 LTS"
echo "- Apache 2.4"
echo "- PHP 7.1 7.2 7.3"
echo "- MySQL $MYSQL_VERSION"
echo "- Composer"
echo "- Magento 2.1, 2.2, 2.3"
echo "=================================================="


echo "=================================================="
echo "SET LOCALES"
echo "=================================================="

export DEBIAN_FRONTEND=noninteractive

export LANGUAGE=en_US.UTF-8
export LANG=en_US.UTF-8
export LC_TYPE=en_US.UTF-8
export LC_ALL=en_US.UTF-8
locale-gen en_US en_US.UTF-8
dpkg-reconfigure locales


echo "=================================================="
echo "RUN UPDATE"
echo "=================================================="

sudo apt-get update
sudo apt-get upgrade


echo "=================================================="
echo "INSTALLING APACHE"
echo "=================================================="

sudo apt-get -y install apache2 libapache2-mod-fcgid

if ! [ -L /var/www ]; then
    rm -rf /var/www
    ln -fs /vagrant /var/www
fi

DEFAULT_VHOST=$(cat <<EOF
    <VirtualHost *:80>
        DocumentRoot "/var/www/src/index/"
        ServerName "$HOST_ALIAS"
        <Directory "/var/www/src/index/">
            AllowOverride All
        </Directory>
        <FilesMatch \.php$>
           SetHandler "proxy:unix:/run/php/php7.3-fpm.sock|fcgi://localhost"
        </FilesMatch>
    </VirtualHost>
EOF
)
sudo echo "$DEFAULT_VHOST" > /etc/apache2/sites-available/000-default.conf

MAGENTO21_VHOST=$(cat <<EOF
    <VirtualHost *:80>
        DocumentRoot "/var/www/src/magento21/pub"
        ServerName "m21.$HOST_ALIAS"
        <Directory "/var/www/src/magento21">
            AllowOverride All
        </Directory>
        <FilesMatch \.php$>
           SetHandler "proxy:unix:/run/php/php7.1-fpm.sock|fcgi://localhost"
        </FilesMatch>
        SetEnv MAGE_IS_DEVELOPER_MODE true
    </VirtualHost>
EOF
)
sudo echo "$MAGENTO21_VHOST" > /etc/apache2/sites-available/magento21.conf

MAGENTO22_VHOST=$(cat <<EOF
    <VirtualHost *:80>
        DocumentRoot "/var/www/src/magento22/pub"
        ServerName "m22.$HOST_ALIAS"
        <Directory "/var/www/src/magento22">
            AllowOverride All
        </Directory>
        <FilesMatch \.php$>
           SetHandler "proxy:unix:/run/php/php7.2-fpm.sock|fcgi://localhost"
        </FilesMatch>
        SetEnv MAGE_IS_DEVELOPER_MODE true
    </VirtualHost>
EOF
)
sudo echo "$MAGENTO22_VHOST" > /etc/apache2/sites-available/magento22.conf

MAGENTO23_VHOST=$(cat <<EOF
    <VirtualHost *:80>
        DocumentRoot "/var/www/src/magento23/pub"
        ServerName "m23.$HOST_ALIAS"
        <Directory "/var/www/src/magento23">
            AllowOverride All
        </Directory>
        <FilesMatch \.php$>
           SetHandler "proxy:unix:/run/php/php7.3-fpm.sock|fcgi://localhost"
        </FilesMatch>
        SetEnv MAGE_IS_DEVELOPER_MODE true
    </VirtualHost>
EOF
)
sudo echo "$MAGENTO23_VHOST" > /etc/apache2/sites-available/magento23.conf

echo "ServerName localhost" | sudo tee /etc/apache2/conf-available/localhost.conf

a2enconf localhost
a2enmod actions fcgid alias proxy_fcgi rewrite
a2ensite magento21
a2ensite magento22
a2ensite magento23
service apache2 restart


echo "=================================================="
echo "INSTALLING PHP"
echo "=================================================="

sudo apt-get -y update
sudo add-apt-repository ppa:ondrej/php
sudo apt-get -y update

PHP_VERSION=7.1
sudo apt-get -y install php"$PHP_VERSION" php"$PHP_VERSION"-fpm php"$PHP_VERSION"-curl php"$PHP_VERSION"-cli php"$PHP_VERSION"-mysql php"$PHP_VERSION"-mcrypt php"$PHP_VERSION"-gd php"$PHP_VERSION"-intl php"$PHP_VERSION"-xsl php"$PHP_VERSION"-zip php"$PHP_VERSION"-mbstring php"$PHP_VERSION"-bcmath php"$PHP_VERSION"-soap
PHP_VERSION=7.2
sudo apt-get -y install php"$PHP_VERSION" php"$PHP_VERSION"-fpm php"$PHP_VERSION"-curl php"$PHP_VERSION"-cli php"$PHP_VERSION"-mysql php"$PHP_VERSION"-gd php"$PHP_VERSION"-intl php"$PHP_VERSION"-xsl php"$PHP_VERSION"-zip php"$PHP_VERSION"-mbstring php"$PHP_VERSION"-bcmath php"$PHP_VERSION"-soap
PHP_VERSION=7.3
sudo apt-get -y install php"$PHP_VERSION" php"$PHP_VERSION"-fpm php"$PHP_VERSION"-curl php"$PHP_VERSION"-cli php"$PHP_VERSION"-mysql php"$PHP_VERSION"-gd php"$PHP_VERSION"-intl php"$PHP_VERSION"-xsl php"$PHP_VERSION"-zip php"$PHP_VERSION"-mbstring php"$PHP_VERSION"-bcmath php"$PHP_VERSION"-soap

phpenmod mbstring
service apache2 restart

echo "=================================================="
echo "INSTALLING COMPOSER"
echo "=================================================="
echo "===== Install composer requirements"
echo "----- Install zip/unzip"
sudo apt-get install zip
sudo apt-get install unzip

echo "----- Install git"
sudo apt-get install git-core

echo "----- Install composer"
curl --silent https://getcomposer.org/installer | php
sudo mv composer.phar /usr/local/bin/composer

cd /vagrant/src

echo "=================================================="
echo "INSTALLING MYSQL and CONFIGURE DATABASE"
echo "=================================================="

sudo debconf-set-selections <<< 'mysql-server mysql-server/root_password password root'
sudo debconf-set-selections <<< 'mysql-server mysql-server/root_password_again password root'
sudo apt-get -y install mysql-server-"$MYSQL_VERSION"

sudo service mysql restart

sed -i '43 s/^/#/' /etc/mysql/my.cnf
sed -i '47 s/^/#/' /etc/mysql/my.cnf

mysql -u root -proot -Bse "CREATE USER 'magento21'@'%' IDENTIFIED BY 'password';
                           GRANT ALL PRIVILEGES ON *.* TO 'magento21'@'%' WITH GRANT OPTION;
                           FLUSH PRIVILEGES;
                           CREATE DATABASE magento21 character set UTF8 collate utf8_bin;"

mysql -u root -proot -Bse "CREATE USER 'magento22'@'%' IDENTIFIED BY 'password';
                           GRANT ALL PRIVILEGES ON *.* TO 'magento22'@'%' WITH GRANT OPTION;
                           FLUSH PRIVILEGES;
                           CREATE DATABASE magento22 character set UTF8 collate utf8_bin;"

mysql -u root -proot -Bse "CREATE USER 'magento23'@'%' IDENTIFIED BY 'password';
                           GRANT ALL PRIVILEGES ON *.* TO 'magento23'@'%' WITH GRANT OPTION;
                           FLUSH PRIVILEGES;
                           CREATE DATABASE magento23 character set UTF8 collate utf8_bin;"


mysql -umagento21 -ppassword magento21 < /var/www/vagrant-provisioning/db/magento21.sql
mysql -umagento22 -ppassword magento22 < /var/www/vagrant-provisioning/db/magento22.sql
mysql -umagento23 -ppassword magento23 < /var/www/vagrant-provisioning/db/magento23.sql

mysql -umagento21 -ppassword magento21 -e "UPDATE core_config_data SET value=\"http://m21.$HOST_ALIAS/\" WHERE path=\"web/unsecure/base_url\"";
mysql -umagento22 -ppassword magento22 -e "UPDATE core_config_data SET value=\"http://m22.$HOST_ALIAS/\" WHERE path=\"web/unsecure/base_url\"";
mysql -umagento23 -ppassword magento23 -e "UPDATE core_config_data SET value=\"http://m23.$HOST_ALIAS/\" WHERE path=\"web/unsecure/base_url\"";

sudo service apache2 restart
sudo service mysql restart

echo "=================================================="
echo "INSTALLING Magento"
echo "=================================================="

cp /vagrant/vagrant-provisioning/env/m21.php /vagrant/src/magento21/app/etc/env.php
cp /vagrant/vagrant-provisioning/env/m22.php /vagrant/src/magento22/app/etc/env.php
cp /vagrant/vagrant-provisioning/env/m23.php /vagrant/src/magento23/app/etc/env.php

cd /vagrant/src/magento21
php7.1 /usr/local/bin/composer install

cd /vagrant/src/magento22
php7.2 /usr/local/bin/composer install

cd /vagrant/src/magento23
php7.3 /usr/local/bin/composer install

php7.1 /var/www/src/magento21/bin/magento module:enable --all
php7.1 /var/www/src/magento21/bin/magento setup:upgrade
php7.1 /var/www/src/magento21/bin/magento setup:di:compile

php7.2 /var/www/src/magento22/bin/magento setup:upgrade
php7.2 /var/www/src/magento22/bin/magento setup:di:compile

php7.3 /var/www/src/magento23/bin/magento setup:upgrade
php7.3 /var/www/src/magento23/bin/magento setup:di:compile


echo "=================================================="
echo "CLEANING"
echo "=================================================="
sudo apt-get -y autoremove
sudo apt-get -y autoclean

echo "=================================================="
echo "============= INSTALLATION COMPLETE =============="
echo "=================================================="