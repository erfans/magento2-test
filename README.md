### Magento 2 test environment 

Including versions 2.1, 2.2, 2.3  
On all versions sample data are installed

### How to install
1. copy `auth.json.sample` to `src/magento21/auth.json`, `src/magento22/auth.json` and `src/magento23/auth.json` 
and fill the required credentials with your magento keys.  
2. run `vagrant up`

### Backend Credentials
username: admin
password: admin123
admin email: admin@domain.com

### PHP Versions
Mangeto 2.1 > PHP7.1  
Mangeto 2.2 > PHP7.2  
Mangeto 2.3 > PHP7.3  

### To run composer commands

```bash
php7.1 /usr/local/bin/composer install
php7.2 /usr/local/bin/composer install
php7.3 /usr/local/bin/composer install
```

### To run Magento commands

```bash
php7.1 /var/www/src/magento21/bin/magento setup:upgrade
php7.1 /var/www/src/magento21/bin/magento setup:di:compile

php7.2 /var/www/src/magento22/bin/magento setup:upgrade
php7.2 /var/www/src/magento22/bin/magento setup:di:compile

php7.3 /var/www/src/magento23/bin/magento setup:upgrade
php7.3 /var/www/src/magento23/bin/magento setup:di:compile
```   